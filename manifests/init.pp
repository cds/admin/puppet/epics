class epics (
    Enum['', 'AUXLAN', 'OPSLAN', 'VACLAN', 'FELAN', 'FMCSLAN', 'SYSLAN', 'WEBLAN'] $subnet = '',
    String $base_version,
    String $package_version,
) {

    if ($base_version != '') {
	package {'epics-base':
	    ensure => $package_version,
	}
    } else {
	file {'/usr/lib64/epics':
	    ensure => absent,
	    target => "/usr/lib64/epics-$base_version",
	}
    }

    if ($subnet != '') {
	file {'/opt/config':
	    ensure => directory,
	    owner => 'root',
	    group => 'root',
	    mode => '0755',
	}
	file {"/opt/config/$subnet":
	    ensure => file,
	    owner => 'root',
	    group => 'root',
	    mode => '0644',
	    content => '',
	}
    } else {
        # TODO: Iterate through all valid Enum values and remove them
	# from /opt/config
    }
}
